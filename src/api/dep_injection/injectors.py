from dependency_injector import containers, providers

from ..application.services.get_inputs import GetInputs
from ..application.services.save_input import SaveInput
from ..application.services.parse_input import ParseInput

from ..infraestructure.repositories.sqlite_tangelo_flattened_input_repo import \
    SqliteTangeloFlattenedInputRepo


class dependencies(containers.DeclarativeContainer):
    app_config = providers.Configuration()
    db_config = providers.Configuration()


    #REPOSITORIES======================================
    tangelo_flattened_input_repo = providers.Singleton(
        SqliteTangeloFlattenedInputRepo,
        db_filename=db_config.sqlite.filename,
        max_results=db_config.sqlite.max_results
    )


    #SERVICES==========================================
    get_tangelo_inputs_srv = providers.Factory(
        GetInputs,
        repo=tangelo_flattened_input_repo
    )

    save_tangelo_input_srv = providers.Factory(
        SaveInput,
        repo=tangelo_flattened_input_repo
    )

    parse_tangelo_input_srv = providers.Factory(
        ParseInput,
        dateformat=app_config.misc.dateformat
    )