from flask_restplus import fields

from ...namespaces import TangeloInputNamespace


TangeloInputRequestModel = {
    'input': TangeloInputNamespace.parser().
                add_argument('input', type=str, help='Tangelo Input', required=True),
    'output': TangeloInputNamespace.parser().
                add_argument('output', type=str, help='Flattened Input', required=True).
                add_argument('depth', type=str, help='Input depth', required=True)
}