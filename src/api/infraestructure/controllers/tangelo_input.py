from flask import request


from flask_restplus import Resource
from dependency_injector.wiring import inject, Provide

from ...dep_injection.injectors import dependencies

from ...application.services.get_inputs import GetInputs
from ...application.services.save_input import SaveInput
from ...application.services.parse_input import ParseInput

from ..namespaces import TangeloInputNamespace
from .request_models.TangeloInputModel import TangeloInputRequestModel

@TangeloInputNamespace.route('/')
class TangeloInput(Resource):
    def __init__(self, api=None, *args, **kwargs):
        super().__init__(api=api, *args, **kwargs)


    @inject
    def get(
        self,
        get_inputs_srv: GetInputs = Provide[dependencies.get_tangelo_inputs_srv]
    ):
        l = get_inputs_srv.execute()
        response = '['
        for ele in l:
            response += '{"input": "' + \
                ele.array_input + '", "output": "' + \
                ele.array_output + '", "datetime": "' + \
                ele.timestamp + '"},'
        response = response[:-1] + ']'
        return response

    @TangeloInputNamespace.expect(
        TangeloInputRequestModel['input']
    )
    def post(
        self,
        save_input_srv: SaveInput = Provide[dependencies.save_tangelo_input_srv],
        parse_input_srv: ParseInput = Provide[dependencies.parse_tangelo_input_srv]
    ):
        flattened = parse_input_srv.execute(request.args['input'])
        save_input_srv.execute(flattened[0])
        return f'flattened_array: {flattened[0].array_output} - depth: {flattened[1]}'
