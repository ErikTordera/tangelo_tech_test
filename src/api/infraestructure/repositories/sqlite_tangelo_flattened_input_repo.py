import sqlite3

from ...domain.contracts.tangelo_flattened_input_repository import TangeloFlattenedInputRepository
from ...domain.entities.tangelo_flattened_input import TangeloFlattenedInput


class SqliteTangeloFlattenedInputRepo(TangeloFlattenedInputRepository):
    def __init__(self, db_filename, max_results):
        self.__db_filename = db_filename
        self.__limit = max_results

    def save(self, flattened_input: TangeloFlattenedInput):
        db = sqlite3.connect(self.__db_filename)

        query = f'INSERT INTO inputs (input, output, datetime) ' +\
                f'VALUES ("{flattened_input.array_input}", "{flattened_input.array_output}", "{flattened_input.timestamp}");'

        cur = db.execute(query)
        cur.fetchall()
        cur.close()

        db.commit()
        db.close()


    def get_all(self):
        db = sqlite3.connect(self.__db_filename)

        query = f'SELECT input, output, datetime FROM inputs ORDER BY datetime DESC LIMIT {self.__limit};'
        cur = db.execute(query)
        rv = cur.fetchall()
        cur.close()

        db.commit()
        db.close()

        return [TangeloFlattenedInput(result[0], result[1], result[2]) for result in rv]

