"""Definition of namespaces
"""
from flask_restplus import Namespace

TangeloInputNamespace = Namespace('TangeloInput', description='Tangelo custom input')
