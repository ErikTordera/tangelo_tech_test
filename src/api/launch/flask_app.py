from flask import Flask
from flask_restplus import resource, Api

from ..dep_injection.injectors import dependencies

from ..infraestructure.namespaces import TangeloInputNamespace
from ..infraestructure.controllers import tangelo_input

__app = None
__api = None
def create_app():
    deps = dependencies()
    deps.app_config.from_yaml('src/config.yaml')
    deps.db_config.from_yaml('src/database_cfg.yaml')

    controllers = [
        tangelo_input
    ]

    deps.wire(
        modules=controllers
    )
    __app = Flask(__name__)
    __app.deps = deps

    __api = Api(
        __app,
        version=deps.app_config.api.title(),
        title=
            f'{__app.deps.app_config.api.title()}' +
            f' - V{__app.deps.app_config.api.version.number()}' +
            f'-{__app.deps.app_config.api.version.name()}',
        description=deps.app_config.api.description()
    )

    __api.add_namespace(TangeloInputNamespace, path='/Input')

    return __app


def get_app():
    return __app


def get_api():
    return __api
