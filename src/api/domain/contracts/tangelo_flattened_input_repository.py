import abc

from ..entities.tangelo_flattened_input import TangeloFlattenedInput

class TangeloFlattenedInputRepository(metaclass=abc.ABCMeta):
    def save(self, flattened_input: TangeloFlattenedInput):
        raise NotImplementedError

    def get_all(self):
        raise NotImplementedError