from ...domain.contracts.tangelo_flattened_input_repository import TangeloFlattenedInputRepository

class GetInputs:
    def __init__(
        self,
        repo: TangeloFlattenedInputRepository
    ):
        self.__repo = repo

    def execute(self):
        return self.__repo.get_all()