import datetime

from ...domain.entities.tangelo_flattened_input import TangeloFlattenedInput

class ParseInput:
    def __init__(
        self,
        dateformat
    ):
        self.dateformat = dateformat
        self.__max_depth = 0


    def execute(self, inp: str):
        now = datetime.datetime.today()
        now = now.strftime(self.dateformat)

        out = self.__flatten(inp)

        return (TangeloFlattenedInput(inp, out[0], now), out[1])


    def __flatten(self, inp: str):
        return (self.__parse(inp), self.__max_depth)


    def __parse(self, inp: str):
        split = inp.replace(' ', '').split(';')
        return self.__sub_parse(split)


    def __sub_parse(self, values: list, from_element_index: int = 0, abs_depth: int = -1):
        out = []
        depth = abs_depth
        if self.__max_depth < abs_depth:
            self.__max_depth = abs_depth

        for index, val in enumerate(values[from_element_index:]):
            if depth == abs_depth:
                #CREATE SUB ARRAY
                if '(' == val[0]:
                    values[from_element_index + index] = val[1:]
                    sub_list = self.__sub_parse(values, from_element_index + index, abs_depth + 1)
                    out = out + sub_list

                else:
                    #RETURN CONTROL
                    if ')' in val:
                        norm = val.replace(')', '')
                        out.append(norm)
                        return out
                    else:
                        out.append(val.replace(')', '').replace('(', ''))

            #KEEP AN EYE ON THE CURRENT DEPTH TO CONTINUE OR STOP INPUTTING VALUES
            if '(' in val:
                for c in val:
                    if c == '(':
                        depth += 1

            if ')' in val:
                for c in val:
                    if c == ')':
                        depth -= 1

        return out