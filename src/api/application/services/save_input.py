from ...domain.entities.tangelo_flattened_input import TangeloFlattenedInput
from ...domain.contracts.tangelo_flattened_input_repository import TangeloFlattenedInputRepository

class SaveInput:
    def __init__(
        self,
        repo: TangeloFlattenedInputRepository
    ):
        self.__repo = repo

    def execute(self, inp: TangeloFlattenedInput):
        self.__repo.save(inp)