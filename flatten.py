inp = '((10 ; ((20 ; (30))) ; (40))) '


def __parse(inp: str):
    split = inp.replace(' ', '').split(';')
    return __sub_parse(split)[0]


def __sub_parse(values: list, from_element_index: int = 0, abs_depth: int = -1):
    out = []
    depth = abs_depth

    for index, val in enumerate(values[from_element_index:]):
        if depth == abs_depth:
            #CREATE SUB ARRAY
            if '(' == val[0]:
                values[from_element_index + index] = val[1:]
                sub_list = __sub_parse(values, from_element_index + index, abs_depth + 1)
                out.append(sub_list)

            else:
                #RETURN CONTROL
                if ')' in val:
                    norm = val.replace(')', '')
                    out.append(norm)
                    return out
                else:
                    out.append(val.replace(')', '').replace('(', ''))

        #KEEP AN EYE ON THE CURRENT DEPTH TO CONTINUE OR STOP INPUTTING VALUES
        if '(' in val:
            for c in val:
                if c == '(':
                    depth += 1

        if ')' in val:
            for c in val:
                if c == ')':
                    depth -= 1

    return out

print(parse(inp))